# Bordeaux model
## Transport flows and housing market

This is a computation tool given as an acoompagniment to the paper
**A model for the evaluation of urban transport policies and their 
interaction with the housing market**

### To use this tool

1. download the package brd.f

        git clone https://gogs.univ-littoral.fr/mkilani/bordeaux-model

2. compile the fortran code

        gfortran -o brd brd.f

   which produces an executable called "brd"

3. run a simulation 

        ./brd empiric/s1.dat


        
