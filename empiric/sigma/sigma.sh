#!/bin/bash
#

f1="../s$1.dat"
f2="sig$1.dat"

./model $f1 > tmp1
./model $f2 > tmp2

diff $f1 $f2 | tail -4
diff -y  tmp1 tmp2 | awk 'NF==9 && (/VP\(/ || /TC\(/) {print $0, "     "$9-$4;next;} {print $0;}' | tail -30

rm -f tmp1 tmp2

